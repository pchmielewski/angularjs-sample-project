﻿(function() {
    'use strict';

    angular
        .module('mainApp').controller("ChoreCtrl", function ($scope) {
            $scope.logChore = function (chore) {
                alert(chore + ' is done!');
            };
        });


    angular
        .module('mainApp')
        .directive('isolationScope', ['$window', isolationScope]);
    
    function isolationScope ($window) {
        // Usage:
        // 
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'E',
            scope: {
                done:"&"
            },
            template: '<input type="text" ng-model="chore"/>' + ' {{chore}}' +
                        '<div class="btn btn-warning" ng-click="done({chore:chore})"> I m done!</div>'
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }

})();