﻿(function() {
    'use strict';

    angular
        .module('mainApp')
        .directive('addClass', ['$window', addClass]);
    
    function addClass ($window) {
        // Usage:
        // 
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'A',
            template: ""
        };
        return directive;

        function link(scope, element, attrs) {
            element.bind("mouseenter", function ()
            {
                element.addClass("bnt btn-danger");
                console.log("mouseenter");
            });
        }
    }

})();