﻿(function() {
    'use strict';

    angular
        .module('mainApp')
        .directive('directiveWithControler', ['$window', directiveWithControler]);
    
    function directiveWithControler($window) {
        // Usage:
        // 
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'E',
            scope: {}, //dyrektywa bedzie miala swoj scope, nie bedzie wspoldzielic z inymi
            controller: function($scope){
                $scope.ab = []

                this.addStrength = function () {
                    $scope.ab.push("strength");
                }

                this.addSpeed = function () {
                    $scope.ab.push("speed");
                }
            }
        };
        return directive;

        function link(scope, element, attrs) {
            element.addClass("btn");
            element.bind("mouseenter", function ()
            {
                console.log(scope.ab);
            })
        }
    }


    angular.module('mainApp').directive("strength", function () {
        return {
            require: "directiveWithControler",
            link: function (scope, element, attrs, di) {
                di.addStrength();
            }
        };
    })

    angular.module('mainApp').directive("speed", function () {
        return {
            require: "directiveWithControler",
            link: function (scope, element, attrs, di) {
                di.addSpeed();
            }
        };
    })

})();
