﻿(function() {
    'use strict';

    angular
        .module('mainApp')
        .directive('moreInfo', ['$window', moreInfo]);
    
    function moreInfo ($window) {
        // Usage:
        // 
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {
                                                    //jest scisle powiazane z kontrolerem AppCtrl
                element.bind("mouseenter", function ()
                {
                    scope.$apply(attrs.moreInfo);
                });
        }
    }

})();