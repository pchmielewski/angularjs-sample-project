﻿(function() {
    'use strict';

    angular
        .module('mainApp')
        .directive('removeClass', ['$window', removeClass]);
    
    function removeClass ($window) {
        // Usage:
        // 
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'A',
            template: ""
        };
        return directive;

        function link(scope, element, attrs) {
            element.bind("mouseleave", function () {
                element.removeClass(attrs.removeClass);
                console.log("mouseleave");
            });
        }
    }

})();