﻿/// <reference path="directive1.js" />
var mainApp = angular.module("mainApp", []);

mainApp.factory('Data', function () {
    return {mes: "Jakas tam wiadomos"};
});

mainApp.factory('Workers', function () {
    var workers = {
        "workers": [
        { "firstName": "John", "lastName": "Doe" },
        { "firstName": "Anna", "lastName": "Smith" },
        { "firstName": "Peter", "lastName": "Jones" }
        ]
    };
    return workers;
});


//Przyklad zastosowania filtow. plus wstrzykniecie obiektu data
mainApp.filter('reverseText', function (Data) {
    return function (text)
    {
        return text.split("").reverse().join("") + " " + Data.mes;
    }
});


mainApp.controller('studentController', function ($scope, Data, Workers) {
    $scope.student = {
        firstName: "Mahesh",
        lastName: "Parashar",

        //Metody robimy tak
        fullName: function () { 
            var studentObject;
            studentObject = $scope.student;
            return studentObject.firstName + " " + studentObject.lastName;
        }
    };

    // Wstrzykujemy tutaj obiekt z fabryki, lapiemy prze mes.mes
    $scope.mes = Data;

    // Tak definujemy metody
    $scope.reverse = function (message) { 
        return message.split("").reverse().join("");
    };

    $scope.workers = Workers.workers;

});

mainApp.directive('helloWorld', function () {
    return {
        restrict: 'AE',
        replace: 'true',
        template: '<h3>Hello World!!</h3>'
    };
});
