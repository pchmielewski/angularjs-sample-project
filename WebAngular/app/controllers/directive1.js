﻿(function() {
    'use strict';

    angular
        .module('mainApp')
        .directive('mouseent', ['$window', directive1]);
    
    function directive1 ($window) {
        // Usage:
        // 
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'EA',
            template: "<div>First directive</div>"
        };
        return directive;

        function link(scope, element, attrs) {
            element.bind("mouseenter", function ()
            {
                console.log("Inside directive");
            })
        }
    }

})();