﻿(function () {
    'use strict';

    angular
        .module('mainApp')
        .controller('appCtrl', ['$scope', appCtrl]);

    function appCtrl($scope) {
        $scope.title = 'appCtrl';

        $scope.loadMoreThx = function()
        {
            alert("Przeplyw pomiedzy dyrektywa a kontrolerem");
        }

        activate();

        function activate() { }
    }
})();
