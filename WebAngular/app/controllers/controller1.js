﻿(function () {
    'use strict'; //http://www.w3schools.com/js/js_strict.asp

    angular
        .module('mainApp')
        .controller('controller1', ['$scope', controller1]);

    function controller1($scope) {
        $scope.danejakies = 'dane jakies';

        activate();

        function activate() { }
    }
})();
